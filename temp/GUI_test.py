from tkinter import filedialog
from tkinter import *
import os
import json
from PIL import Image, ImageTk


import auto_thresh
import tiff_tile

def browse_folder_button():
    filename = filedialog.askdirectory()
    folder_path.set(filename)

def browse_output_button():
    filename = filedialog.askdirectory()
    output_path.set(filename)

def produce_image():
    auto_thresh.find_overlaps(folder_path.get(), img_list)
    tiff_tile.produce_tiff(os.path.join(folder_path.get(), 'split'))

def clear_browse():
    folder_path.set("")

def populate_img_list():
    try:
        img_list = ([f for f in os.listdir(folder_path.get()) if f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif'))])
        img_var.set(img_list)
    except:
        img_var.set("")

    popup = Toplevel(root)

    scrollbar = Scrollbar(popup)
    scrollbar.pack(side=RIGHT, fill=Y)

    checklist = Text(popup, width=20)
    checklist.pack()

    global var 
    checkbutton = {}
    var = {}
    vars = []
    for i in img_list:
        var[i] = IntVar()
        vars.append(var[i])
        checkbutton[i] = Checkbutton(checklist, text=i, variable=var[i])
        checkbutton[i].select()
        checklist.window_create("end", window=checkbutton[i])
        checklist.insert("end", "\n")

    checklist.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=checklist.yview)

    # disable the widget so users can't insert text into it
    checklist.configure(state="disabled")

    button1 = Button(popup, text="Close", command=lambda: popup.destroy())
    button1.pack()

# def accept_images():
#     for i in var.keys():
#         print(i, var[i].get())

root = Tk()

root.geometry("700x350")

img_list = []
folder_path = StringVar()
img_var = StringVar(value=img_list)
Label(root, text='Folder path of images to tile').grid(row=1)
e1 = Entry(root, textvariable=folder_path, width=40)
e1.grid(row=1, column=1, padx=1, pady=10)
button2 = Button(text="Browse", command=browse_folder_button)
button2.grid(row=1, column=2, padx=10, pady=10)
button3 = Button(text="Clear", command=clear_browse)
button3.grid(row=1, column=3, padx=10, pady=10)
button4 = Button(text="Load", command=populate_img_list)
button4.grid(row=1, column=4, padx=10, pady=10)

Label(root, text='Prioritise speed or memory?').grid(row=2)
priority = IntVar()
priority.set(1)
Radiobutton(root, text='Speed', variable=priority, value=1).grid(row=2, column=1)
Radiobutton(root, text='Memory', variable=priority, value=2).grid(row=2, column=2)

output_path = StringVar()
Label(root, text='Output location').grid(row=3)
e2 = Entry(root, textvariable=output_path, width=40)
e2.grid(row=3, column=1, padx=1, pady=10)
button5 = Button(text="Browse", command=browse_output_button)
button5.grid(row=3, column=2, padx=10, pady=10)

Label(root, text='Output format').grid(row=4, column=0)
format = IntVar()
format.set(1)
Radiobutton(root, text='JPEG', variable=format, value=1).grid(row=4, column=1)
Radiobutton(root, text='Pyramidal TIFF', variable=format, value=2).grid(row=4, column=2)

Label(root, text='Preview before saving high res?').grid(row=5, column=0)
preview = IntVar()
preview.set(1)
Radiobutton(root, text='Yes', variable=preview, value=1).grid(row=5, column=1)
Radiobutton(root, text='No', variable=preview, value=2).grid(row=5, column=2)

buttonRun = Button(text='Run', command= produce_image)
buttonRun.grid(row=6, column=3)

mainloop()