import numpy as np
import cv2
import os
import sys
import json

def split_image(filepath, tile_size):
    folder_path = os.path.dirname(filepath)

    img = cv2.imread(filepath, -1)

    height, width = img.shape[:2]
    locations = {}
    for i in range(height // tile_size + 1):
        for j in range(width // tile_size + 1):
            y_min = i*tile_size
            if y_min + tile_size > height:
                y_max = height
            else:
                y_max = y_min + tile_size

            x_min = j*tile_size
            if x_min + tile_size > width:
                x_max = width
            else:
                x_max = x_min + tile_size

            img_slice = img[y_min:y_max, x_min:x_max]
            cv2.imwrite(os.path.join(folder_path, 'split', f'{i}_{j}.png'), img_slice)
            
            locations[f'{i}_{j}.png'] = [y_min, x_min]

    with open(os.path.join(folder_path, 'split', 'locations.json'), 'w') as fp:
        json.dump(locations, fp)

if __name__ == "__main__":
    filepath = sys.argv[1]
    tile_size = sys.argv[2]

    split_image(filepath, int(tile_size))