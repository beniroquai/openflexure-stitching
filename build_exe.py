import os
import PyInstaller.__main__
from openflexure_stitching.stitching.find_vips import find_vips_and_add_to_path
from glob import glob

if __name__ == "__main__":
    # vips_paths = ["."]
    # print(f"Searching {vips_paths}")
    # find_vips_and_add_to_path(vips_paths)
    # print(f"VIPS is importable!")

    vips_dlls = glob("vips-*/bin/*.dll")
    vips_dlls += glob("vips-*/bin/vips-modules-*/*.dll")
    print(f"Need to add VIPS DLLs: {vips_dlls}")
    print(f"PATH is {os.environ['PATH']}")

    binaries = []
    for dll in vips_dlls:
        f = os.path.split(dll)[1]
        binaries.append("--add-binary")
        binaries.append(f"{dll}{os.pathsep}.")

    print(f"binaries: {binaries}")

    PyInstaller.__main__.run([
        './pyinstaller/openflexure-stitch.py',
        '--onefile',
        *binaries,
    ])