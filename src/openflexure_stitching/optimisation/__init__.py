"""
Methods to optimise the positions of our tiles
"""
import numpy as np
from tqdm.auto import tqdm
from typing import Dict, List, Optional, Tuple, Union
from ..types import PairData, PairIndices, XYDisplacementInPixels


def stage_correlation_disparity(p: PairData) -> float:
    """Difference between stage displacements and image displacements"""
    displacement = (
        np.array(p.image_displacement)
        - np.array(p.stage_displacement)
    )
    return np.sqrt(np.sum(displacement**2))


def peak_quality(p: PairData) -> float:
    """The quality of the correlation peak"""
    return p.fraction_under_threshold[0.9]


def error_metrics(pairs: List[PairData]) -> List[Tuple[float]]:
    """Stage/correlation disparity and peak quality per pair"""
    return [
        (peak_quality(p), stage_correlation_disparity(p))
        for p in pairs
    ]


def pair_displacements(
        pairs: List[PairData], 
        positions: Union[List[XYDisplacementInPixels], np.ndarray],
    ) -> np.ndarray:
    """Calculate the displacement between each pair of positions specified.
    
    Arguments:
    pairs: [(int,int)]
        A list of tuples, where each tuple describes two images that overlap.
    positions: numpy.ndarray
        An Nx2 array, giving the 2D position in pixels of each image.
    
    Result: 
        The 2D displacement between each pair of images.
    """
    positions = np.asarray(positions)
    return np.array([positions[p.indices[1],:]-positions[p.indices[0],:] for p in pairs])


def fully_connected(accepted_pairs: List[PairData], all_pairs: List[PairData]) -> bool:
    """Determine whether the pairs connect all N images"""
    all_images = [i.indices for i in all_pairs]
    all_images = set([i for j in all_images for i in j])
    total_images = len(all_images)
    assert total_images > 0 
    connected_images = set()
    previous_n_connected = 0
    connected_images.add(0)
    # Each iteration, add any images that are connected to
    # the set - we stop when no more images are added.
    while len(connected_images) > previous_n_connected:
        previous_n_connected = len(connected_images)
        for p in accepted_pairs:
            for i in range(2):
                if p.indices[i] in connected_images:
                    connected_images.add(p.indices[1-i])
    return len(connected_images) == total_images


def rms_error(
        accepted_pairs: List[PairData], 
        positions: List[XYDisplacementInPixels], 
        all_pairs: List[PairData],
        print_err: bool = False
    ) -> float:
    """Find the RMS error in image positons vs correlated displacements
    
    Arguments:
    accepted_pairs: 
        The output from correlating pairs of images, filtered by thresholds
    positions: 
        The postition of each image in pixels, relative to the mean
    all_pairs:
        Every pair of overlapping images, unfiltered
    print_err: bool
        If true, print the RMS error to the console.
    
    Returns: float
        The RMS difference between the displacements calculated from the given
        positions and the displacements specified.
    """
    if len(accepted_pairs) <= len(positions) - 1:
        return float("infinity")
    elif not fully_connected(accepted_pairs, all_pairs):
        return float("infinity")
    displacements = np.array([p.image_displacement for p in accepted_pairs])
    error = np.std(
        pair_displacements(accepted_pairs, positions)
        - displacements, 
        ddof=2*len(positions) - 2
    )  # NB we are taking stdev of an array with 2*N elements, if we have N pairs.
    # We then fit 2*len(positions)-2 parameters
    if print_err:
        print ("RMS Error: %.2f pixels" % error)
    return error


def filter_pairs(pairs: List[PairData], quality_threshold: float, disparity_threshold: float) -> List[PairData]:
    """Filter pairs against thresholds for the two error metrics."""
    return [
        p for p in pairs
        if (
            peak_quality(p) >= quality_threshold
            and stage_correlation_disparity(p) <= disparity_threshold
        )
    ]


def images_in_pairs_list(pairs: List[PairData]) -> set[int]:
    """Find all images mentioned in a list of pairs"""
    return set(i for p in pairs for i in p.indices)


def fit_positions_lstsq(
        pairs: List[PairData], 
        stage_pairs: Optional[List[PairData]] = None,
        stage_weighting: float = 0.001,
        return_A_matrix: bool = False,
        missing_images_to_nan: bool = True,
    ) -> np.ndarray:
    """Find the least squares solution for image placement.
    
    Placing N images such that the pair-wise displacements match those measured
    can be formulated as a linear least squares problem, because for each 
    measured displacement, it can be written as a dot product between a vector
    with exactly two nonzero elements (one +1 and one -1) and the position 
    vector.

    This function calculates a matrix A such that each row of A is such a vector,
    and then uses that, together with the measured displacements, to recover the
    positions. In order to make sure the problem is constrained, we add an 
    extra two rows, constraining the mean position to be zero. We could trivially
    modify this to set an arbitrary mean position.

    If `pairs` does not connect all the images, this will be ill-posed and we'll 
    have a problem. `fallback_pairs` allows us to pass in additional pair data
    that will be used with a low weighting (`fallback_weighting`) to break the
    ambiguity. Typically this is generated from stage coordinates, for images
    that otherwise would be left floating.
    
    Arguments:
    pairs: 
        The results of correlating images against each other
    fallback_pairs:
        Additional pair data to be used with a low weighting
    
    Returns: numpy.ndarray
        The optimal positions array.
    """
    if stage_pairs is None:
        stage_pairs = []
    # N = np.max([p.indices for p in accepted_pairs]) + 1  # The highest index plus one gives #images
    all_images = sorted(images_in_pairs_list(pairs + stage_pairs))
    N = len(all_images)
    M = len(pairs + stage_pairs)

    displacements = np.array([p.image_displacement for p in pairs])
    stage_displacements = np.array([p.stage_displacement for p in stage_pairs])
    # Our A matrix will have 2(M+1) rows, x and y constraints for each pair, plus x and y means
    d = np.concatenate([
        np.reshape(displacements, 2*len(pairs)),
        np.reshape(stage_displacements, 2*len(stage_pairs)) * stage_weighting,
        np.array([0,0]),
    ])
    A = np.zeros((2*M + 2, 2*N), dtype=float)
    # The last two rows sum up the x and y coordinates respectively
    for i in range(2):
        A[2*M + i, i::2] = 1/N
    # For the other rows, we take the difference between pairs of images
    for i, p in enumerate(pairs):
        # Note that a, b are the indices in `all_images`, which may not be complete, hence
        # the need to do a look-up each time.
        a, b = (all_images.index(ind) for ind in p.indices)
        for j in range(2):  # j selects x then y
            A[2*i + j, 2*a + j] = -1
            A[2*i + j, 2*b + j] = 1
    i0 = len(pairs)
    for i, p in enumerate(stage_pairs):
        # Note that a, b are the indices in `all_images`, which may not be complete, hence
        # the need to do a look-up each time.
        a, b = (all_images.index(ind) for ind in p.indices)
        for j in range(2):  # j selects x then y
            A[2*(i + i0) + j, 2*a + j] = -1 * stage_weighting
            A[2*(i + i0) + j, 2*b + j] = 1 * stage_weighting

    if return_A_matrix:
        return A

    # Now, solve the least squares problem
    positions, _ssr, _rank, _singular = np.linalg.lstsq(A, d, rcond=None)
    fitted_positions = positions.reshape((N, 2))
    if not missing_images_to_nan:
        return fitted_positions
    # If there were images missing from `pairs`, we can ensure indices in the returned
    # coordinates match image indices by padding with NaN.
    all_positions = np.empty((np.max(all_images) + 1, 2))
    all_positions[...] = np.nan
    for i, j in enumerate(all_images):
        all_positions[j, :] = fitted_positions[i, :]
    return all_positions



def test_threshold(
        pairs: List[PairData], 
        x_thresh: float, 
        y_thresh: float,
        cache: Optional[Dict[Tuple[PairIndices], float]]
    ) -> Tuple[float, int]:
    """Evaluate the RMS error when we use a particular threshold
    
    Optionally, a cache dictionary can be supplied, in which case
    we'll use it to avoid solving for any subset of the pairs more
    than once.
    """
    accepted_pairs = filter_pairs(pairs, x_thresh, y_thresh)
    if len(accepted_pairs) == 0:
        return float("infinity"), 0 # Clearly we can't throw away all the pairs!
    cache_key = tuple(p.indices for p in accepted_pairs)
    if cache and cache_key in cache:
        # print("Saved a call to fit_positions_lstsq using the cache")
        return cache[cache_key], len(accepted_pairs)
    positions = fit_positions_lstsq(accepted_pairs)
    error = rms_error(accepted_pairs, positions, pairs)
    if cache is not None:
        cache[cache_key] = error
    return error, len(accepted_pairs)

def manual_threshold():
    x_thresh = float(input('Insert x threshold: '))
    y_thresh = float(input('Insert y threshold: '))
    return x_thresh, y_thresh

def test_all_thresholds(pairs: List[PairData]):
    """Try all possible threshold values for the error metrics"""
    # Pick unique, monotonic values for the two thresholds
    all_x = sorted(list(set([peak_quality(p) for p in pairs])))
    all_y = sorted(list(set([stage_correlation_disparity(p) for p in pairs])))
    tested = []
    cached_errors: Dict[Tuple[PairIndices], float] = {}
    for x_thresh in tqdm(all_x, desc="Auto-thresholding correlations"):  # iterate over peak quality, lax to strict
        this_col = []
        y_thresh = np.max(all_y)
        error, n_pairs = test_threshold(pairs, x_thresh, y_thresh, cache=cached_errors)
        tested.append([x_thresh, y_thresh, error, n_pairs])
        if error == float("infinity"):
            #print(f"Peak quality threshold {x_thresh} doesn't fully constrain positions, skipping.")
            continue
        for y_thresh in all_y:
            error, n_pairs = test_threshold(pairs, x_thresh, y_thresh, cache=cached_errors)
            tested.append([x_thresh, y_thresh, error, n_pairs])
            this_col.append(error)
            if len(this_col) >= 2 and this_col[-1] - this_col[-2] > 0:
                # We keep adding in more pairs (increasing y_thresh) until the
                # error starts to increase.
                break
    
    x_vals, y_vals, errors, n_pairs = zip(*tested)
    min_i = np.argmin(errors) if np.min(errors) < float("infinity") else np.argmax(n_pairs)

    x_thresh = x_vals[min_i]
    y_thresh = y_vals[min_i]

    return x_thresh, y_thresh
