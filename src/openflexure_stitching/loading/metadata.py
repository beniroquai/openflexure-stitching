"""
Manage metadata associated with images

This tiling system currently expects images to have metadata in the
format used by OpenFlexure (a JSON string in the usercomment EXIF
field). It would be lovely to generalise this!
"""

from datetime import datetime
import json
import numpy as np
import os
import re
import piexif
import cv2

from typing import Dict, List, Any
from ..types import ImageMetadata


def read_metadata_from_file(fname: str, first_file=False) -> ImageMetadata:
    """Read the important metadata from an image file
    
    Currently this expects OpenFlexure-fromat metadata, which is stored
    as a JSON string in the UserComment EXIF field. In the future, we should
    extend this function to use other metadata standards such as OME-XML
    or OME-TIFF.
    """
    ctime = os.path.getctime(fname)
    try:
        exif = piexif.load(fname)
    except:
        exif = None
    width, height = get_img_size(fname, exif, first_file)
    try:
        usercomment = exif_usercomment_json(exif)
        try:
            pixel_size_um = usercomment['instrument']['settings']['extensions']['usafcal']['um_per_px']
        except:
            pixel_size_um = 0
        if "instrument" in usercomment:
            return ImageMetadata(
                filename = fname,
                camera_to_sample_matrix = np.array(
                    usercomment['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement']
                ),
                ctime = ctime,
                timestamp = datetime.strptime(usercomment['image']['time'], '%Y-%m-%dT%H:%M:%S.%f'),
                tags = usercomment['image']['tags'],  # an empty list will be OK here
                stage_position = [
                    int(usercomment['instrument']['state']['stage']['position'][dim])
                    for dim in ['x', 'y', 'z']
                ],
                width = width,
                height = height,
                pixel_size_um = pixel_size_um,
            )
        elif "/stage/" in usercomment:
            # print(usercomment['/camera_stage_mapping/']['image_to_stage_displacement_matrix'])
            return ImageMetadata(
                filename = fname,
                camera_to_sample_matrix = np.array(
                    usercomment['/camera_stage_mapping/']['image_to_stage_displacement_matrix']
                ),
                ctime = ctime,
                timestamp = datetime.strptime(exif['Exif'][piexif.ExifIFD.DateTimeOriginal].decode(), '%Y:%m:%d %H:%M:%S'),
                tags = [],  # an empty list will be OK here
                stage_position = [
                    int(usercomment['/stage/']['position'][dim])
                    for dim in ['x', 'y', 'z']
                ],
                width = width,
                height = height,
                pixel_size_um = pixel_size_um,
            )
        else:
            raise KeyError("Did not recognise the EXIF UserComment on the image")
    except:
        # Fall back to reading from the file directly
        # This section could use more work...
        m = re.match(".*_(-?\d+)_(-?\d+)_(-?\d+)\..*", fname)
        stage_position = [int(d) for d in m.groups()]
        return ImageMetadata(
            filename = fname,
            camera_to_sample_matrix=None,
            ctime = ctime,
            timestamp = ctime,
            tags = [],
            stage_position = stage_position,
            width = width,
            height = height,
            pixel_size_um = 0,
        )

def get_img_size(fname:str, metadata: Dict = None, first_file=False):
    if not metadata:
        try:
            metadata = piexif.load(fname)
        except:
            metadata = None
    try:
        width = metadata["Exif"][piexif.ExifIFD.PixelXDimension]
        height = metadata["Exif"][piexif.ExifIFD.PixelYDimension]
    except:
        if first_file:
            img = cv2.imread(fname, -1)
            height, width = img.shape[:2]
        else:
            width, height = 0, 0
    return width, height

def replace_csm(metadata_list: List[ImageMetadata], camera_to_sample_matrix: np.ndarray):
    "Replace the camera_to_sample_matrix (editing in-place)"
    for m in metadata_list:
        m.camera_to_sample_matrix = camera_to_sample_matrix


def scale_csm(metadata_list: List[ImageMetadata], calibration_width: int):
    "Account for a calibration width that may differ from image width"
    scale = metadata_list[0].width / calibration_width  # Usually >1, if we calibrated at low res
    csm = metadata_list[0].camera_to_sample_matrix / scale  # Decrease the CSM if pixels are smaller]
    replace_csm(metadata_list=metadata_list, camera_to_sample_matrix=csm)


def exif_usercomment_json(exif_dict: Dict[str, Any]) -> Dict:
    """Extract the OpenFlexure metadata from a usercomment dict"""
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        return json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
    else:
        raise KeyError("There is no EXIF UserComment on the image")