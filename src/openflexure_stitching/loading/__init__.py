# -*- coding: utf-8 -*-
"""
Manage the input of folders of images, including caching metadata

@author: rwb27 and jaknapper
"""

import numpy as np
import os

from typing import List, Optional

from .metadata import read_metadata_from_file, scale_csm, get_img_size
from ..types import ImageMetadata, TilingInputs, PerImageInputs, TilingSettings


def load_images(
        folder: str,
        tiling_settings: TilingSettings
    ) -> TilingInputs:
    """Load images from disk, check metadata, and optionally stitch open-loop"""
    fnames = find_images(folder)
    image_metadata = load_metadata_for_images(folder, fnames, tiling_settings)
    if tiling_settings.csm_calibration_width == -1:
        print(
            "WARNING: we are assuming that the camera_to_sample_matrix "
            "was calibrated with images that are 832 pixels wide, and we "
            "will scale it accordingly if your images are not this size. "
            "This behaviour is appropriate for images from the OpenFlexure "
            "microscope using software v2.11.1 and earlier. To prevent this "
            "behaviour, use --csm_calibration_width 0"
        )
        tiling_settings.csm_calibration_width = 832
    if tiling_settings.csm_calibration_width > 0:
        scale_csm(image_metadata, calibration_width=tiling_settings.csm_calibration_width)
    return tiling_inputs_from_metadata(image_metadata, folder), image_metadata[0].camera_to_sample_matrix, tiling_settings.csm_calibration_width


def find_images(folder_path: str) -> List[str]:
    """List all images in a folder, excluding ones that are output."""

    # Get a list of all images in the folder, excluding ones containing any of 'ignore_phrases'
    ignore_phrases = ['stitched', 'comparison', 'Fused', 'stage', 'stitching']

    return [
        f for f in os.listdir(folder_path) 
        if os.path.isfile(os.path.join(folder_path,f)) 
        and f.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')) 
        and not any(phrase in f for phrase in ignore_phrases)
    ]

def load_metadata_for_images(
        folder_path: str,
        images: List[str],
        tiling_settings: TilingSettings,
        filter_sample: bool=True
    ) -> List[ImageMetadata]:
    """Load metadata for a list of images, in a standardised form
    
    This function will load metadata using `.metadata.read_metadata_from_file`, see
    that function for details of the metadata format.

    Arguments:
    * `filter_sample`: if `True` (default), it will look for a `sample` tag in the
      metadata of each image. If any images are tagged as `sample`, only images with
      that tag will be returned in the metadata list. If there are no images tagged
      as `sample` this option has no effect.
    * `camera_to_sample_matrix` will be used to overwrite the CSM matrix in the image
      metadata, if it's present.
    """
    metadata_list: List[ImageMetadata] = [
        read_metadata_from_file(
            os.path.join(folder_path, image_fname),
            first_file = (image_fname == images[0]),
        )
        for image_fname in images
    ]
    metadata_list.sort(key=lambda x: x.timestamp, reverse=False)
    # Overwrite with the specified CSM if one has been set
    if tiling_settings.csm_matrix != [[0,0],[0,0]]:
        for m in metadata_list:
            m.camera_to_sample_matrix = np.array(tiling_settings.csm_matrix)

    # Some scans will mark images as either sample or background. If some are marked as sample, only tile those
    if filter_sample:
        sample_list = [m for m in metadata_list if "sample" in m.tags]
        if len(sample_list) > 0:
            metadata_list = sample_list

    #TODO: this function previously grouped images by XY position and filtered only the sharpest.
    # RWB removed this function as it wasn't being used.

    return metadata_list


def tiling_inputs_from_metadata(
        image_metadata: List[ImageMetadata], 
        folder: str
    ) -> TilingInputs:
    """Estimate pixel positions, based on the stage_position of each image

    The input to the rest of the tiling process requires an estimate of
    the relative position of each image, in pixels. We calculate this
    from the stage coordinates and a transform matrix, detailed below.
    
    We position the images as per their stage coordinates, after applying
    the inverse of the `camera_to_sample_matrix` to convert stage coords
    into pixels on the camera. The `camera_to_sample_matrix` is either
    taken from the argument with that name or, if it is None, is loaded
    from the first image in the metadata list.

    Returns an Nx2 `np.array` of positions, in units of pixels.

    Note: this function is unaware of any intent to resize the images, so
    the pixels are whatever the images were calibrated against. If the
    calibration was performed using a different image size, you will need
    to scale these numbers.
    """
    camera_to_sample_matrix = image_metadata[0].camera_to_sample_matrix
    if camera_to_sample_matrix is None:
        raise ValueError(
            "No camera to sample mapping matrix was supplied, and "
            "we could not load one from the first image's metadata."
        )
    stage_positions = [m.stage_position[:2] for m in image_metadata]
    centre = np.mean(stage_positions, axis=0)[:2]
    rel_positions = np.array(stage_positions) - centre
    pixel_positions = (
        np.dot(
            rel_positions[:,:2], 
            np.linalg.inv(camera_to_sample_matrix)
        )
    )
    return TilingInputs(
        images = [
            PerImageInputs(
                filepath = m.filename,
                timestamp = m.timestamp,
                position_from_stage = tuple(pixel_positions[i, :]),
                stage_position = stage_positions[i]
            )
            for i, m in enumerate(image_metadata)
        ],
        image_shape = (image_metadata[0].height, image_metadata[0].width),
        folder = folder,
        pixel_size_um = image_metadata[0].pixel_size_um
    )

