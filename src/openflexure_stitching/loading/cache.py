"""
Manage cache files that include correlation results, metadata, etc.
"""

import json
import os

from json import JSONDecodeError
from pydantic import RootModel, ValidationError
from pydantic.tools import parse_obj_as
from typing import List, Tuple
# from ..correlation import stage_displacement
from ..types import CorrelationSettings, PairData, PairIndices, TilingCache, TilingInputs, XYDisplacementInPixels


def cache_path(folder_path: str):
    """The location where we store the metadata cache"""
    return os.path.join(folder_path, 'tiling_cache.json')


def write_cache(folder_path: str, data: TilingCache):
    """Save the cache to the default location"""
    with open(cache_path(folder_path), "w") as f:
        f.write(RootModel[TilingCache](data).model_dump_json(indent=4))


def read_cache(tiling_inputs: TilingInputs) -> TilingCache:
    """Read the cache from the default location"""
    try:
        with open(cache_path(tiling_inputs.folder), "r") as f:
            cache_dict = json.load(f)
            return parse_obj_as(TilingCache, cache_dict)
    except FileNotFoundError:
        raise CacheFailureException("No cache file found.")
    except JSONDecodeError:
        raise CacheFailureException("The cache file was not valid JSON")
    except ValidationError:
        raise CacheFailureException("The cache file had the wrong structure.")
    
def name_from_path(filepath: str) -> str:
    """Get the filename from its path, dependent of OS"""
    return os.path.normpath(filepath).split(os.path.sep)[-1]

def check_tiling_inputs_match(cache: TilingCache, tiling_inputs: TilingInputs):
    """Check the cached tiling inputs match the supplied ones
    
    NB we deliberately don't worry if one or the other has extra images,
    because in that case we will load them automatically
    """
    for m1, m2 in zip(tiling_inputs.images, cache.tiling_inputs.images):
        failure = 0
        if name_from_path(m1.filepath) != name_from_path(m2.filepath):
            failure = 1
        elif m1.timestamp != m2.timestamp:
            failure = 1
        elif m1.stage_position != m2.stage_position:
            failure = 1
        elif m1.position_from_stage != m2.position_from_stage:
            # print(f"Centre of scan has changed, but we can still use the offset")
            pass
        if failure:
            raise CacheFailureException(
                "Image metadata in the cache file did not match."
            )
        else:
            print(f"We can reuse {m1.filepath} from the cache")

class CacheFailureException(Exception):
    pass


def get_pairs_from_cache(
        cache: TilingCache,
        tiling_inputs: TilingInputs,
        settings: CorrelationSettings, 
    ) -> List[PairData]:
    cache = read_cache(tiling_inputs=tiling_inputs)
    check_tiling_inputs_match(cache=cache, tiling_inputs=tiling_inputs)
    # Check if the settings match and return those pairs
    for cache_settings, pair_data in cache.correlations:
        if cache_settings == settings:
            for pair in pair_data:
                i, j = pair.indices[0], pair.indices[1]
                pair.stage_displacement = tuple(stage_displacement(tiling_inputs, i, j))
            return pair_data
    raise CacheFailureException("The settings have not been used before.")

def stage_displacement(inputs: TilingInputs, i: int, j: int) -> XYDisplacementInPixels:
    """Displacement in pixels from image i to j estimated from stage coords"""
    import numpy as np
    return (
        np.array(inputs.images[j].position_from_stage)
        - np.array(inputs.images[i].position_from_stage)
    )


def split_overlapping_pairs(
        overlapping_pairs: List[PairIndices], 
        cached_pairs: List[PairData]
    ) -> Tuple[List[PairData], List[PairIndices]]:
    """Look up cached pairs, and determine pairs still to correlate"""
    cache_hits: List[PairData] = []
    pairs_to_correlate: List[PairIndices] = []
    for i in overlapping_pairs:
        matches = [p for p in cached_pairs if p.indices == i]
        if len(matches) == 1:
            # print(f"We're using the cache for {i}")
            cache_hits.append(matches[0])
        elif len(matches) == 0:
            # print(f"We need to generate {i}")
            pairs_to_correlate.append(i)
        else:
            raise ValueError(f"The cache contained {len(matches)} entries for {i}")
    return cache_hits, pairs_to_correlate


def lookup_pairs_from_cache(
        tiling_inputs: TilingInputs, 
        settings: CorrelationSettings,
        overlapping_pairs: List[PairIndices],
    ) -> Tuple[List[PairData], List[PairIndices]]:
    try:
        cache = read_cache(tiling_inputs=tiling_inputs)
        cached_pairs = get_pairs_from_cache(
            cache=cache,
            tiling_inputs=tiling_inputs,
            settings=settings,
        )
        return split_overlapping_pairs(
            overlapping_pairs=overlapping_pairs,
            cached_pairs=cached_pairs
        )
    except CacheFailureException as e:
        print(f"Could not load cached correlations ({e}), starting from scratch")
        return [], overlapping_pairs
    

def save_pairs_to_cache(
        tiling_inputs: TilingInputs,
        settings: CorrelationSettings,
        pair_data: List[PairData]
    ) -> None:
    """Save pair data to the cache file
    
    This will add the correlations to those already in the cache,
    so we can easily switch back and forth between settings.
    """
    try:
        cache = read_cache(tiling_inputs=tiling_inputs)
        matches = [  # filter out correlations that match current settings
            (s, pd) for (s, pd) in cache.correlations 
            if s==settings
        ]
        if len(matches) > 1:
            raise CacheFailureException(
                "Multiple caches for current settings should not happen - we will overwrite the cache."
            )
    except CacheFailureException:
        cache = TilingCache(
            tiling_inputs=tiling_inputs,
            correlations = []
        )
        matches = cache.correlations  # This is empty - no need to filter, but this gets type right
    if matches:
        # If we do have cached correlations, make sure we retain any that we didn't recalculate.
        # This avoids deleting things from the cache, if we try to reconstruct a subset of the
        # images.
        previous_correlations = matches[0][1]
        new_indices = [pair.indices for pair in pair_data]
        retained_correlations = [pair for pair in previous_correlations if pair.indices not in new_indices]
        cache.correlations.append((settings, retained_correlations + pair_data))
    else:
        cache.correlations.append((settings, pair_data))
    write_cache(folder_path=tiling_inputs.folder, data=cache)