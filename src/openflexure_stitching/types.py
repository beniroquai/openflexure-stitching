"""
Data types to keep our algorithm organised

This uses `pydantic.dataclasses.dataclass` to ease serialising and loading from cache.
"""

import numpy as np
from datetime import datetime
from pydantic import (
    BeforeValidator,
    BaseModel,
    ConfigDict,
    PlainSerializer,
    RootModel,
    WithJsonSchema,
)
from typing import Annotated, Dict, List, Literal, Optional, Tuple, Union

# Define a nested list of floats with 0-6 dimensions
# This would be most elegantly defined as a recursive type
# but the below gets the job done for now.
Number = Union[int, float]
NestedListOfNumbers = Union[
    Number,
    List[Number],
    List[List[Number]],
    List[List[List[Number]]],
    List[List[List[List[Number]]]],
    List[List[List[List[List[Number]]]]],
    List[List[List[List[List[List[Number]]]]]],
    List[List[List[List[List[List[List]]]]]],
]
class NestedListOfNumbersModel(RootModel):
    root: NestedListOfNumbers

def np_to_listoflists(arr: np.ndarray) -> NestedListOfNumbers:
    """Convert a numpy array to a list of lists
    
    NB this will not be quick! Large arrays will be much better
    serialised by dumping to base64 encoding or similar.
    """
    return arr.tolist()

def listoflists_to_np(lol: NestedListOfNumbers) -> np.ndarray:
    """Convert a list of lists to a numpy array"""
    return np.asarray(lol)

# Define an annotated type so Pydantic can cope with numpy
NDArray = Annotated[
    np.ndarray,
    BeforeValidator(listoflists_to_np),
    PlainSerializer(np_to_listoflists, when_used="json-unless-none"),
    WithJsonSchema(NestedListOfNumbersModel.model_json_schema(), mode="validation"),
]

class ImageMetadata(BaseModel):
    """This dataclass represents the metadata for one image."""
    model_config = ConfigDict(arbitrary_types_allowed=True)
    filename: str
    camera_to_sample_matrix: Optional[NDArray]
    timestamp: datetime
    ctime: datetime
    tags: List[str]
    stage_position: List[int]
    width: int
    height: int
    pixel_size_um: float = 0

PairIndices = Tuple[int, int]  # Used in various places to indicate a pair of images
XYDisplacementInPixels = Tuple[float, float]


class PerImageInputs(BaseModel):
    filepath: str
    timestamp: datetime
    position_from_stage: XYDisplacementInPixels
    stage_position: List


class TilingInputs(BaseModel):
    """The minimal information required to perform tiling.
    
    This includes the filenames of the images, the folder where
    they are located, and the shape of each image.
    """
    folder: str
    images: List[PerImageInputs]
    image_shape: Tuple[int, int]
    pixel_size_um: float = 0

class TilingSettings(BaseModel):
    """The settings for tiling"""
    csm_matrix: List
    csm_calibration_width: float
    thresholding_method: Literal["automatic", "manual"] = "automatic"

class PairData(BaseModel):
    """The results of correlating images together
    
    For convenience this also includes an estimate of the displacement between
    images as estimated from the stage coordinates and the transform matrix.
    """
    indices: PairIndices
    image_displacement: XYDisplacementInPixels
    stage_displacement: XYDisplacementInPixels
    fraction_under_threshold: Dict[float, float] # The proportion of pixels in the correlation image beneath a threshold
    

class CorrelationSettings(BaseModel):
    """Input parameters that affect the determination of image-to-image displacements"""
    pad: Annotated[bool, "Whether to zero-pad before Fourier transforming. False (default) prioritises speed over ambiguity"] = False
    resize: Annotated[float, "Images are scaled by this factor before performing FFT."] = 1.0
    high_pass_sigma: Annotated[float, "strength of the high pass filter to apply (larger number = more low frequencies cut)"] = 50
    minimum_overlap: Annotated[float, "minimum overlap area to count as overlapping, as a fraction of the image"] = 0.2
    priority: Literal["time", "memory"] = "time"


class TilingCache(BaseModel):
    tiling_inputs: TilingInputs
    correlations: List[Tuple[CorrelationSettings, List[PairData]]]
