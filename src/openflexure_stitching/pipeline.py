from .loading import load_images
from .correlation import correlate_overlapping_images
from .types import CorrelationSettings, TilingSettings, TilingInputs, PairData
from . import plotting
from .optimisation import test_all_thresholds, filter_pairs, fit_positions_lstsq, manual_threshold
from .stitching import stitch_and_save, stitch_to_tiles
from .loading import get_img_size
try:
    from .stitching.pyramidal_tiff import produce_tiff
    TIFF_ENABLED = True
except ImportError:
    print("VIPS not installed: TIFF export disabled")
    TIFF_ENABLED = False
from typing import Optional
from argparse import ArgumentParser, Namespace, BooleanOptionalAction
import os
import ast
import numpy as np


def load_tile_and_stitch(
        folder: str,
        correlation_settings: Optional[CorrelationSettings],
        tiling_settings: Optional[TilingSettings],
        plot_inputs: bool = True,
        plot_correlations: bool = True,
        stitch_from_stage: bool = True,
        stitch_from_correlations: bool = True,
    ):
    """Load images, correlate them together, and output stitched images
    
    This is the top-level function that goes from a folder of input
    images to a """
    tiling_inputs, starting_csm, csm_calibration_width = load_images(
        folder=folder,
        tiling_settings=tiling_settings
    )
    if plot_inputs:
        f = plotting.plot_inputs(tiling_inputs)
        f.savefig(os.path.join(folder, "stitching_inputs.png"))
    if stitch_from_stage:
        stitch_and_save("stitched_from_stage.jpg", tiling_inputs, None)
    pairs = correlate_overlapping_images(
        tiling_inputs=tiling_inputs, settings=correlation_settings
    )
    if plot_correlations or tiling_settings.thresholding_method == "manual":
        f = plotting.plot_overlaps(pairs)
        f.savefig(os.path.join(folder, "stitching_correlations.png"))
    
    if tiling_settings.thresholding_method == 'automatic':
        x_thresh, y_thresh = test_all_thresholds(pairs)
    else:
        x_thresh, y_thresh = manual_threshold()

    if plot_correlations:
        f = plotting.plot_overlaps(pairs, x_thresh, y_thresh)
        f.savefig(os.path.join(folder, "stitching_correlations.png"))

    filtered_pairs = filter_pairs(pairs, x_thresh, y_thresh)
    print(f"Fitting {len(filtered_pairs)} out of {len(pairs)} correlations")
    #for p in filtered_pairs:
    #     print(f"{p.image_displacement} vs {p.stage_displacement}")
    transform = fit_affine_transform(filtered_pairs)
    improved_csm = (np.linalg.inv(np.matmul(np.linalg.inv(starting_csm),np.linalg.inv(transform))))
    rejected_pairs = [p for p in pairs if p not in filtered_pairs]
    transformed_rejected_pairs = apply_affine_transform(rejected_pairs, transform)
    positions = fit_positions_lstsq(filtered_pairs, stage_pairs=transformed_rejected_pairs)
    if stitch_from_correlations:
        stitch_and_save("stitched.jpg", tiling_inputs=tiling_inputs, positions=positions, downsample=1)
        stitch_to_tiles("tiles", tiling_inputs=tiling_inputs, positions=positions, downsample=1)
        if TIFF_ENABLED:
            produce_tiff(os.path.join(folder, "tiles"), os.path.join(folder, "stitched.ome.tiff"))

    if csm_calibration_width != 0:
        scale = get_img_size(tiling_inputs.images[0].filepath)[0] / csm_calibration_width
    else:
        scale = 1

    improved_csm = (scale * improved_csm).tolist()
    
    print(f'A better starting estimate of the csm is {improved_csm}. You don\'t need to update the width')



def apply_affine_transform(
        pairs: list[PairData],
        transform: np.ndarray,
) -> list[PairData]:
    """Apply an affine transform to the stage coordinates in pair data"""
    inverse_transform = np.linalg.inv(transform)
    return [
        PairData(
            indices = p.indices,
            image_displacement = p.image_displacement,
            stage_displacement = tuple(np.dot(p.stage_displacement, inverse_transform)),
            fraction_under_threshold = p.fraction_under_threshold,
        )
        for p in pairs
    ]


def load_tile_and_stitch_cli():
    parser = ArgumentParser(
        description="Tile together a folder of microscope images."
    )
    add_tiling_args(parser)
    add_correlation_args(parser)
    parser.add_argument("--skip_stage_coords", action="store_true", help="Don't stitch an image from stage coordinates.")
    args = parser.parse_args()
    correlation_settings = correlation_settings_from_args(args)
    tiling_settings = tiling_settings_from_args(args)
    load_tile_and_stitch(
        folder=args.input_folder,
        correlation_settings=correlation_settings,
        tiling_settings=tiling_settings,
        stitch_from_stage=not args.skip_stage_coords,
    )

def add_tiling_args(p: ArgumentParser):
    """Add arguments associated with loading the images"""
    p.add_argument("input_folder", help="A folder containing images to tile.")
    p.add_argument(
        "--csm_calibration_width", 
        type=float,
        default=-1,
        help=(
            "The width of the images used to calibrate the CSM. "
            "If you calibrated the camera_to_sample_matrix at a lower "
            "resolution than your images, you may specify it here, so "
            "that we can rescale it appropriately. If the matrix is "
            "appropriate to use as-is, pass 0 to this argument. "
            "WARNING: by default this is set to 832, which is the "
            "right value to use for OpenFlexure images. This will "
            "change in the future once OpenFlexure passes enough "
            "metadata to determine this automatically."
        )
    )
    p.add_argument(
        "--thresholding_method", 
        type=str,
        default="automatic",
        help=(
            "The method to identify thresholds to apply to your tiling."
            "Options are 'automatic' (default) or 'manual'. "
            "Manual is quicker, but requires you to examine "
            "'stitching_correlations.png' to choose your x and y cutoffs."
        )
    )
    p.add_argument(
        "--csm_matrix", 
        type=str,
        default="[[0,0],[0,0]]",
        help=(
            "The matrix from camera-stage-mapping calibration. "
            "By default, this is [[0,0],[0,0]] and will instead be loaded "
            "from your images automatically. If you are confident you need "
            "to overwrite it (for example, if a previous scan suggested"
            "an improved version), specify it as '[a , b], [c , d]'"
        )
    )


def add_correlation_args(p: ArgumentParser):
    """Add arguments associated with image correlation"""
    #TODO: swap this for pydantic-argparse?
    p.add_argument(
        "--pad",
        default=True,
        action=BooleanOptionalAction,
        help="Whether to zero-pad to remove ambiguity when correlating images (default: True)."
    )
    # TODO: decide whether resize/downsample makes sense as an option
    p.add_argument(
        "--high_pass_sigma", 
        type=float,
        default=50,
        help=(
            "Strength of the high pass filter applied before cross-correlation. "
            "This is the standard deviation of a Gaussian, in units of FFT "
            "pixels, i.e. larger number = stronger high pass filter. Default is "
            "50."
        )
    )
    p.add_argument(
        "--minimum_overlap",
        type=float,
        default=0.2,
        help=(
            "The minimum fractional overlap between two images required for correlation. "
            "Default value is 0.2. This is calculated as the area of the overlap "
            "divided by the area of one image."
        )
    )
    p.add_argument(
        "--priority",
        type=str,
        default="time",
        help=(
            "Whether to prioritise time or memory footprint when optimising the image cache. "
            "'time' caches images as long as possible, while 'memory' will load images from "
            "the filesystem each time they are required."
        ),
    )


def correlation_settings_from_args(a: Namespace) -> CorrelationSettings:
    """Process correlation-related arguments and return settings"""
    s = CorrelationSettings(
        pad=a.pad,
        resize=1.0,
        high_pass_sigma=a.high_pass_sigma,
        minimum_overlap=a.minimum_overlap,
        priority=a.priority,
    )
    return s

def tiling_settings_from_args(a: Namespace) -> TilingSettings:
    """Process tiling-related arguments and return settings"""
    s = TilingSettings(
        csm_matrix=ast.literal_eval(a.csm_matrix),
        csm_calibration_width = a.csm_calibration_width,
        thresholding_method = a.thresholding_method
    )
    return s

def fit_affine_transform(
        correlations: list[PairData],
    ):
    """Find an affine transform to convert from stage positions to pixel.
    
    Find an affine tranform (i.e. 2x2 matrix) that, when applied to the 
    stage coordinates (in `correlations`), matches the measured pair-wise
    displacements in `correlations` as closely as possible.
    This can be used to refine the camera-stage-mapping matrix.
    
    Arguments:
    Result: numpy.ndarray, numpy.ndarray
        A 2x2 matrix that transforms the given stage coordinates to match the
        pixel positions.
    """
    image_displacements = np.array([c.image_displacement for c in correlations])
    stage_displacements = np.array([c.stage_displacement for c in correlations])
    

    affine_transform = np.linalg.lstsq(image_displacements, stage_displacements,
                                       rcond=-1)[0]

    return affine_transform