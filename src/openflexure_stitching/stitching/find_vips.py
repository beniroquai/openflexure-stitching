"""
`pyvips` requires a copy of `libvips` on the PATH. This may be satisfied
if it's installed and correctly configured, which is likely on Linux
systems. If not, it's possible we have a local copy and so we should 
have a look for it.

`find_vips_and_add_to_path` is the useful function in this module.

There's not a built-in option to install VIPS, because that's not easy
in a platform-independent way (if it was, I assume `pyvips` would do it).
However, if we have bundled it into an executable, we should also have
bundled an appropriate `libvips`, and this function can add it to the
PATH so it imports correctly.
"""

from typing import List, Optional
from contextlib import contextmanager
import os

def find_vips_and_add_to_path(folders_to_search: Optional[List[str]]=None):
    """Locate libvips such that `import pyvips` will work
    
    First, we try to import `pyvips` and if that succeeds, we're done.
    Next, we will try the folders specified, in order, to see if they
    contain an installation of `pyvips`. The folder should be the
    vips `bin` directory, its parent directory, or a directory that
    contains exactly one folder prefixed with `vips-` (in which the
    bin directory resides).
    """
    if folders_to_search is None:
        folders_to_search = []
    try:
        import_pyvips()
        return  # if it imports successfully, no need to try other paths!
    except OSError:
        # The rest of this function is effectively the exception handler
        pass
    stack = list(reversed(folders_to_search))
    while len(stack) > 0:
        f = stack.pop()
        try:
            # Pick likely-looking subfolders and try them as well
            candidates = [
                os.path.join(f, d)
                for d in os.listdir(f)
                if os.path.isdir(os.path.join(f, d)) and (
                    d.startswith("vips-") 
                    or d == "bin"
                )
            ]
        except FileNotFoundError:
            continue  # no point testing folders that don't exist!
        if candidates:
            stack += candidates  # we'll investigate subfolders first
            #continue # if there's a bin or vips- subfolder, don't test this folder.
        try:
            with temporarily_add_to_path(f):
                import_pyvips()
            add_to_path(f)
            return
        except OSError:
            continue
    raise ModuleNotFoundError("pyvips could not load. Most likely libvips is not installed.")


def import_pyvips() -> bool:
    """Check whether `pyvips` can be imported."""
    try:
        import pyvips
        return True
    except ModuleNotFoundError as e:
        # If it's just not installed, we can't fix that - so 
        # we raise a hopefully-helpful error
        raise ModuleNotFoundError(
            f"{e.msg}\n"
            f"You probably need to install pyvips."
        )
    # We don't handle the OSError that will be raised if libvips
    # is missing - leave that for the next level out.


def add_to_path(folder: str):
    """Add a folder to the PATH"""
    os.environ['PATH'] = folder + ';' + os.environ['PATH']


@contextmanager
def temporarily_add_to_path(folder: str):
    """Add a folder to the path, removing it afterwards"""
    prev_path = os.environ['PATH']
    try:
        add_to_path(folder)
        yield
    finally:
        os.environ['PATH'] = prev_path
