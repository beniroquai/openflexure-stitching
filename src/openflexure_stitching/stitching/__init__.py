"""
Stitch images together, once you have determined their positions
"""

import numpy as np
import cv2
from typing import List, Tuple, Optional, Sequence
from pydantic import BaseModel
from tqdm.auto import tqdm

import os
import json

from .utils import overlap_slices, arange_from_slice, downsample_image, regions_overlap, RegionOfInterest
from ..types import TilingInputs, XYDisplacementInPixels 

def stitch_and_save(
        filename: str, 
        tiling_inputs: TilingInputs, 
        positions: Optional[Sequence[XYDisplacementInPixels]],
        downsample: int = 3,
    ):
    """Stitch images together and save to a file"""
    if positions is None:
        positions = [i.position_from_stage for i in tiling_inputs.images]
    inputs = calculate_stitch_geometry(
        tiling_inputs,
        downsample=downsample,
        positions=positions
    )
    img = stitch_images(inputs)
    cv2.imwrite(os.path.join(tiling_inputs.folder, filename), img)


def stitch_to_tiles(
        foldername: str, 
        tiling_inputs: TilingInputs, 
        positions: Optional[Sequence[XYDisplacementInPixels]],
        downsample: int = 3,
    ):
    """Stitch images together and save to a file"""
    if positions is None:
        positions = [i.position_from_stage for i in tiling_inputs.images]
    geometry = calculate_stitch_geometry(
        tiling_inputs,
        downsample=downsample,
        positions=positions
    )
    stitch_geometry_to_tiles(
        os.path.join(tiling_inputs.folder, foldername),
        g=geometry,
        tile_size=(4096, 4096),
        pixel_size_um=tiling_inputs.pixel_size_um * downsample,
    )

    

class StitchGeometry(BaseModel):
    # When we place these, they must go at whole-pixel coordinates. These
    # are calculated once, here, for consistency.
    files: List[str]
    downsample: int = 1
    image_size: Tuple[int, int]
    positions: List[XYDisplacementInPixels]
    output_size: Tuple[int, int]
    output_centre: Tuple[float, float]
    
    def image_centre(self, index: int) -> np.ndarray:
        """The position of the centre of an image, in stitched image coordinates"""
        return (
            (np.array(self.positions[index]) - np.array(self.output_centre))/self.downsample
            + np.array(self.output_size)/2.0
        )

    def top_left(self, index: int, quantize=True) -> np.ndarray:
        """Calculate the top-left coordinate of an image"""
        topleft = self.image_centre(index) - np.array(self.image_size)/2.0/self.downsample
        if quantize:
            return np.ceil(topleft).astype(int)
        else:
            return topleft
        
    def image_shift(self, index: int) -> np.ndarray:
        """How much the image should be shifted during downsampling
        
        The input images are placed into the output at integer pixel positions: if the calculated
        position is not an integer, we should shift the image while we downsample it, to get 
        sub-pixel positioning.
        """
        return (
            self.top_left(index, quantize=True) 
            - self.top_left(index, quantize=False)
        ) * self.downsample
    
    _downsampled_image_size = None

    @property
    def downsampled_image_size(self) -> Tuple[int, int]:
        """The size of the downsampled images"""
        if not self._downsampled_image_size:
            self._downsampled_image_size = [d // self.downsample - 1 for d in self.image_size]
        return self._downsampled_image_size

    @property
    def n_images(self):
        """The number of input images"""
        return len(self.files)
    
    @property
    def indices(self):
        """A range that will enumerate the images"""
        return range(self.n_images)


def calculate_stitch_geometry(
        tiling_inputs: TilingInputs,
        downsample: int = 1,
        positions: Optional[Sequence[XYDisplacementInPixels]] = None,
    ) -> StitchGeometry:
    """Calculate where each image should go in the stitched image
    
    We determine the size of the output image, and the position of each
    image within that.
    """
    files = [i.filepath for i in tiling_inputs.images]
    image_size = tiling_inputs.image_shape
    if positions is None:
        positions = [i.position_from_stage for i in tiling_inputs.images]
    output_size = np.round(
        (np.max(positions, axis=0) - np.min(positions, axis=0)
        + image_size)/downsample
    )
    output_centre = (np.max(positions, axis=0) + np.min(positions, axis=0))/2
    return StitchGeometry(
        files=files,
        downsample=downsample,
        image_size=image_size,
        positions=positions,
        output_size=tuple(output_size),
        output_centre=tuple(output_centre),
    )


def stitch_images(
        inputs: StitchGeometry,
        method: str = "nearest",
        region_of_interest: Optional[RegionOfInterest] = None,
    ) -> np.ndarray:
    """Merge images together, using supplied positions (in pixels).
    
    Place images into a combined image whole. The order of the images
    determines which image is on top, i.e. we will use the last image
    that overlaps each point, not the closest.

    This method is fastest, but will not produce the best stitched images
    as the quality is generally better in the middle of each tile.
    
    Arguments:
    inputs: a StitchGeometry object initialised with the tile filenames
        and positions.
    method: str="closest"
        If this is "closest" each pixel in the final image comes from the tile
        with the centre closest to that pixel, i.e. we use the middle part of
        each image. If it's "latest", we place the images in whole, which is
        faster but uses the edges of later images rather than the centre of
        each image - i.e. the resulting stitched image will be less uniform.
    region_of_interest:
        An optional tuple of (x, y), (width, height)) to output. If specified,
        we will calculate only a subset of the output image.

    Note:
    
    inputs.downsample: int=3
        The size of the stitched image will be reduced by this amount in each
        dimension, to save on resources.  NB currently it decimates rather than
        taking a mean, for speed - in the future a mean may be an option.
        Images are downsampled after taking into account their position, i.e.
        if you downsample by a factor of 5, you'll still be within 1 original
        pixel, not within 5, of the right position.  Currently we don't do
        any sub-pixel shifting.
        
    Returns: (stitched_image, stitched_centre, image_centres)
        (numpy.ndarray, numpy.ndarray, numpy.ndarray)
        An MxPx3 array containing the stitched image, a 1D array of length
        2 containing the coordinates of the centre of the image in non-
        downsampled pixel coordinates, and an Nx2 array of the positions of the
        source images (their centres) relative to the top left of the stitched
        image, in downsampled pixels.
    """
    if not region_of_interest:
        region_of_interest = ((0, 0), inputs.output_size)
    canvas_origin = np.array(region_of_interest[0]).astype(int)
    canvas_size = np.array(region_of_interest[1]).astype(int)
    stitched_image = np.zeros(tuple(canvas_size) + (3,), dtype=np.uint8)
    [inputs.image_centre(i) for i in inputs.indices]
    for i, filename in enumerate(tqdm(inputs.files, "Stitching images")):
        image_roi = (tuple(inputs.top_left(i)), inputs.downsampled_image_size)
        if not regions_overlap(region_of_interest, image_roi):
            continue  # Don't load images we don't need.
        tile = cv2.imread(filename, -1)
        img = downsample_image(tile, inputs.downsample, shift=inputs.image_shift(i))
        tl = inputs.top_left(i, quantize=True) - canvas_origin  # top left, relative to the ROI
        br = tl + np.array(img.shape[:2]) # bottom right, relative to the ROI
        canvas_slices = tuple(  # This is the region of the canvas that overlaps with img
            slice(max(0, tl[d]), min(canvas_size[d], br[d]))
            for d in range(2)
        )
        img_slices = tuple(  # This is the region of img that overlaps with the canvas
            slice(max(0, -tl[d]), min(canvas_size[d], br[d]) - tl[d])
            for d in range(2)
        )
        if method == "latest":
            stitched_image[canvas_slices] = img[img_slices]
        elif method == "nearest":
            mask_image(img, i, inputs)
            stitched_image[canvas_slices] += img[img_slices]
    return stitched_image


def crop_roi(roi: RegionOfInterest, canvas_size: Tuple[int, int]):
    """Crop a region of interest so it fits inside a canvas
    
    Region of interest is a tuple of ((x, y), (width, height))
    """
    pos, size = roi
    if any(pos[d] > canvas_size[d] for d in range(2)):
        raise ValueError("The region of interest is outside the canvas!")
    new_size = tuple(  # Crop the ROI so it fits inside the canvas
        min(size[d], canvas_size[d] - pos[d])
        for d in range(2)
    )
    return pos, new_size


def stitch_geometry_to_tiles(
        output_folder: str,
        g: StitchGeometry,
        tile_size: Tuple[int, int],
        pixel_size_um: float = 0,
    ):
    """Stitch the image and write a series of tiles to a folder, in PNG format"""
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)
    tile_rois = [
        crop_roi(((i*tile_size[0], j*tile_size[1]), tile_size), g.output_size)
        for i in range(int(np.ceil(g.output_size[0] / tile_size[0])))
        for j in range(int(np.ceil(g.output_size[1] / tile_size[1])))
    ]
    for roi in tqdm(tile_rois, desc="Generating TIFF tiles"):
        tile = stitch_images(g, region_of_interest=roi)
        cv2.imwrite(os.path.join(output_folder, f'{roi[0][0]}_{roi[0][1]}.png'), tile)
    with open(os.path.join(output_folder, "metadata.json"), "w") as f:
        json.dump({"pixel_size_um": pixel_size_um}, f)


def mask_image(img: np.ndarray, index: int, inputs: StitchGeometry):
    """Set pixels in an image to zero if the centre of another image is closer
    
    This will result in the edges of an image being set to zero. This means
    that when all images are summed in place, only one image at any point will
    be nonzero.
    
    WARNING: `img` will be modified.
    """
    centre = inputs.image_centre(index)
    for j in inputs.indices:
        other_centre = inputs.image_centre(j)
        if np.all(other_centre == centre):
            # don't compare to this image
            continue
        difference = (
            inputs.top_left(j, quantize=True)
            - inputs.top_left(index, quantize=True)
        )
        if np.any(np.abs(difference) > img.shape[:2]):
            # ignore images that don't overlap with this one
            continue
        xr, yr = overlap_slices(difference, img.shape[:2])
        if xr is None or yr is None:
            print("Warning, empty overlap range unexpected")
            continue
        # Calculate the midpoint between image centres, relative to the
        # top left of img.
        # This is relative to the quantized top left position.
        midpoint = (other_centre + centre)/2.0 - inputs.top_left(index, quantize=True)
        mask = (
            arange_from_slice(xr)[:, np.newaxis] * difference[0]
            + arange_from_slice(yr)[np.newaxis, :] * difference[1]
        ) > np.dot(midpoint, difference)
        img[xr, yr][mask] = 0

