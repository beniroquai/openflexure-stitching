import os
import os.path
import re
import json
from tqdm.auto import tqdm
from .find_vips import find_vips_and_add_to_path

find_vips_and_add_to_path([".", "../../.."])
import pyvips

def produce_tiff(
        tile_folder: str,
        output_filepath: str,
    ):
    """Produce a pyramidal TIFF file from a folder of images"""
    matches = [re.match("(\d+)_(\d+).png", f) for f in os.listdir(tile_folder)]
    tile_fpaths = [os.path.join(tile_folder, m.group(0)) for m in matches if m]
    positions = [(int(m.group(1)), int(m.group(2))) for m in matches if m]
    stitched_img = pyvips.Image.black(1, 1,bands=3) 
    for (y, x), fname in tqdm(zip(positions, tile_fpaths), desc="Combining tiles into TIFF"):
        tile = interleaved_tile(fname, stitched_img.bands)
        stitched_img = stitched_img.insert(tile, x, y, expand=1, background=[0])
    with open(os.path.join(tile_folder, "metadata.json"), "r") as f:
        metadata = json.load(f)
    pixel_size = metadata["pixel_size_um"]
    if pixel_size <= 0:
        print("WARNING: no pixel size information. Scale bars will be wrong.")
        pixel_size = 1
    save_ometiff(stitched_img, output_filepath, pixel_size=pixel_size)


def interleaved_tile(filePath,channels):
    """Chop up an image to interleave bands
    
    TODO: figure out if the interleaved tiling actually does anything..."""
    tile = pyvips.Image.new_from_file(filePath)
    #page_height = tile.height
    ## chop into pages
    #pages = [tile.crop(0, y, tile.width, page_height) for y in range(0, tile.height, page_height)]
    ## join pages band-wise to make an interleaved image
    #tile = pages[0].bandjoin(pages[1:])
    #tile = tile.copy(interpretation="multiband")
    return tile


def save_ometiff(stitched_img, outFile, pixel_size):
    # collect image dimension needed for OME-XML before separating image planes
    width = stitched_img.width
    height = stitched_img.height
    bands = stitched_img.bands

    # split to separate image planes and stack vertically for OME-TIFF 
    stitched_img = pyvips.Image.arrayjoin(stitched_img.bandsplit(), across=1)
    res = 10**3 / pixel_size
    pixel_size /= 10**3
    #Set tiff tags necessary for OME-TIFF
    stitched_img = stitched_img.copy(xres=res, yres=res)

    # build minimal OME metadata. TODO: get calibration and channel names
    stitched_img.set_type(pyvips.GValue.gstr_type, "image-description",
    f"""<?xml version="1.0" encoding="UTF-8"?>
    <OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2016-06"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2016-06 http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd">
        <Image ID="Image:0">
            <!-- Minimum required fields about image dimensions -->
            <Pixels DimensionOrder="XYCZT"
                    ID="Pixels:0"
                    PhysicalSizeX= "{pixel_size}"
                    PhysicalSizeY= "{pixel_size}"
                    PhysicalSizeXUnit= "mm"
                    PhysicalSizeYUnit= "mm"
                    SizeC="{bands}"
                    SizeT="1"
                    SizeX="{width}"
                    SizeY="{height}"
                    SizeZ="1"
                    Type="float">
            </Pixels>
        </Image>
    </OME>""")

    stitched_img.set_type(pyvips.GValue.gint_type, "page-height", height)
    stitched_img.write_to_file(outFile, compression="lzw", tile=True, tile_width=512, tile_height=512,  pyramid=True, subifd=True, bigtiff=True)
