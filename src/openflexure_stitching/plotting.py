"""
A collection of methods to plot data using matplotlib

Plotting is intentionally kept separate from the rest of
the stitching and tiling process to avoid hard dependencies
on matplotlib and make sure code can run headless.
"""

import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib import patches
import numpy as np
from .types import PairData, TilingInputs
from .optimisation import error_metrics
from typing import List, Optional

def plot_overlaps(pairs: List[PairData],
                  x_thresh: Optional[float] = None,
                  y_thresh: Optional[float] = None
                  ) -> Figure:
    """Plot the correlations between images"""
    f, ax = plt.subplots()
    metrics = np.array(error_metrics(pairs))
    ax.plot(metrics[:,0], metrics[:,1], "o")
    if x_thresh:
        ax.axvline(x_thresh)
    if y_thresh:
        ax.axhline(y_thresh)
    if x_thresh and y_thresh:
        ax.add_patch(
            patches.Rectangle(
            xy=(x_thresh, y_thresh),  # point of origin.
            width=1-x_thresh, height=-(y_thresh), linewidth=0,
            fill=True, facecolor = (0,1,0, 0.3))
        )
    ax.set_xlabel('Peak quality (higher is better)')
    ax.set_ylabel('Stage - correlation discrepancy /px \n (lower is better)')
    f.tight_layout()
    return f

def plot_inputs(inputs: TilingInputs) -> Figure:
    """Plot the coordinates of images
    
    NB because of the differing conventions between matplotlib and opencv,
    the y axis is in fact the **first** coordinate and the x axis is the 
    **second**. Also, the y axis runs backwards. This is just to make the
    two match up, so the graph resembles the stitched image.
    """
    f, ax = plt.subplots()
    ax.plot(
        [i.position_from_stage[1] for i in inputs.images],
        [i.position_from_stage[0] for i in inputs.images],
        "o-"
    )
    shape = inputs.image_shape
    for image in inputs.images:
        centre_pos = [i - s/2 for i, s in zip(image.position_from_stage, shape)]
        ax.add_patch(
            patches.Rectangle(
                tuple(reversed(centre_pos)),
                shape[1],
                shape[0],
                linewidth=1,
                edgecolor='b', facecolor=(0,0,1.0,0.2)
            )
        )
    ax.invert_yaxis()
    ax.set_xlabel("Position element [1]/pixels")
    ax.set_ylabel("Position element [0]/pixels")
    ax.set_aspect(1.0)
    return f