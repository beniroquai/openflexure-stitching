import logging
import os
import cv2
import numpy as np
import json
import piexif
import piexif.helper
import reconstruct_tiled_image
import matplotlib.pyplot as plt
import sys
from memory_profiler import memory_usage, profile
import stitching_methods
import time
from alive_progress import alive_bar
import datetime

def test_thresh(pairs, pixel_positions, tiles, displacements):
    t2 = time.time()
    # if len(pairs) < len(tiles):
    #     return 999
    all_sites = reconstruct_tiled_image.sites_linked(pairs, len(tiles), print_res = False)
    if all_sites is False:
        return 999
    # t3 = time.time()
    pixel_positions = reconstruct_tiled_image.fit_positions_lstsq(pairs, displacements)
    t4 = time.time()
    return(reconstruct_tiled_image.rms_error(pairs, pixel_positions, displacements, print_err=True))

def pull_usercomment_dict(filepath):
    """
    Reads UserComment Exif data from a file, and returns the contained bytes as a dictionary.
    Args:
        filepath: Path to the Exif-containing file
    """
    try:
        exif_dict = piexif.load(filepath)
    except piexif._exceptions.InvalidImageDataError:
        logging.warning("Invalid data at {}. Skipping.".format(filepath))
        return None
    if "Exif" in exif_dict and piexif.ExifIFD.UserComment in exif_dict["Exif"]:
        try:
            return json.loads(exif_dict["Exif"][piexif.ExifIFD.UserComment].decode())
        except json.decoder.JSONDecodeError:
            logging.error(
                f"Capture {filepath} has old, corrupt, or missing OpenFlexure metadata. Unable to reload to server."
            )
    else:
        return None

def find_overlaps(folder_path, img_list=None):
    if not img_list:
        img_list = reconstruct_tiled_image.get_img_list(folder_path)
    data = pull_usercomment_dict(os.path.join(folder_path, img_list[0]))
    camera_to_sample = np.array(data['instrument']['settings']['extensions']['org.openflexure.camera_stage_mapping']['image_to_stage_displacement'])

    downsample = 1
    resize = 1
    unknown_images = "warn"
    fractional_overlap= 0.2
    img_limit = 0
    priority = 'time'
    pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre = reconstruct_tiled_image.reconstruct_tiled_image(folder_path, img_list,
                                            camera_to_sample=camera_to_sample, resize = resize, unknown_images = unknown_images, fractional_overlap= fractional_overlap, img_limit = img_limit, pad=False, use_cache=False, priority=priority)

    all_x = []
    all_y = []
    for h, (i, j) in enumerate(pairs):
        
        original_displacement = np.round(pixel_positions[j,:2] - pixel_positions[i,:2])

        data = pair_data['pairs'][f'{i}, {j}']
        all_x.append(data['0.9 thresh'])

        transform = np.array((np.array(data['peak loc']) - original_displacement))
        all_y.append(np.sum(np.abs(transform)))

    # x_thresh, y_thresh = autothresh(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, img_limit, downsample, all_x, all_y)
    x_thresh, y_thresh = allthresh(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, img_limit, downsample, all_x, all_y)

    # reconstruct_tiled_image.plot_overlaps(pairs, pair_data, pixel_positions, block = False)

    # fig, ax = plt.subplots()
    # im = ax.scatter(x_vals[mask], y_vals[mask], c = z_vals[mask])
    # mask = np.where(z_vals == 999)
    # ax.scatter(x_vals[mask], y_vals[mask], c = 'red')

    # for i,j,k in zip(x_vals, y_vals, z_vals):
    #     ax.annotate(str(k),xy=(i,j), xytext=(0,10), textcoords='offset points')

    # fig.colorbar(im, orientation='vertical')
    # plt.show(block=True)

    print(x_thresh, y_thresh)
    print(camera_to_sample)
    tiles, positions, csm, scan_centre = reconstruct_tiled_image.stitch_from_thresholds(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, x_thresh, y_thresh, img_limit, downsample)
    print(csm)
    # t1 = time.time()
    # reconstruction = reconstruct_tiled_image.generate_stitched_image(tiles, positions, resize, downsample, csm, scan_centre)

    # cv2.imwrite(os.path.join(folder_path, f"stitched_auto.jpg"), cv2.cvtColor(reconstruction['stitched_image'][:,:,::-1], cv2.COLOR_BGR2RGB))
    # t2 = time.time()
    # del reconstruction
    stitching_methods.tiff_centre_of_images(tiles, positions/resize, 1, priority=priority)
    # downsample = 1
    # stitched_image = stitching_methods.stitch_centre_of_images(tiles, positions / resize, downsample)[0]
    # cv2.imwrite(os.path.join(folder_path, f"stitched_smoothed_auto.jpg"), cv2.cvtColor(stitched_image[:,:,::-1], cv2.COLOR_BGR2RGB))
    # t3 = time.time()
    # stitched_image = stitching_methods.old_stitch_centre_of_images(tiles, positions / resize, downsample)[0]
    # cv2.imwrite(os.path.join(folder_path, f"old_stitched_smoothed_auto.jpg"), cv2.cvtColor(stitched_image[:,:,::-1], cv2.COLOR_BGR2RGB))
    # t4 = time.time()
    locations = {}
    # print(scan_centre)
    for i, img in enumerate(img_list):
        locations[img] = list(positions[i] / resize + [scan_centre[0], scan_centre[1]])

    with open(os.path.join(folder_path, 'locations.json'), 'w') as fp:
        json.dump(locations, fp)

def autothresh(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, img_limit, downsample, all_x, all_y):
    auto_thresh_dict = {}

    all_x = sorted(list(set(all_x)))
    all_y = sorted(list(set(all_y)))

    print(len(pairs))

    reconstruct_tiled_image.plot_overlaps(pairs,pair_data,pixel_positions)

    to_plot = []
    
    all_x_test = all_x.copy()
    x_index = len(all_x_test) // 2
    while True:
        x_index = len(all_x_test) // 2
        x_thresh = all_x_test[x_index]

    # for x_thresh in x_all_sampled:
        all_y_test = all_y.copy()
        while True:
            y_index = len(all_y_test) // 2

            y_thresh = np.sum(np.abs(all_y_test[y_index]))

            # print(y_index)

            # print(x_thresh, y_thresh)

            # print(x_thresh, y_thresh)
            auto_thresh_dict[f'{x_thresh}, {y_thresh}'] = test_thresh(pairs, pair_data, pixel_positions, tiles, x_thresh, y_thresh)
            to_plot.append([x_thresh, y_thresh, auto_thresh_dict[f'{x_thresh}, {y_thresh}']])
            if len(all_y_test) == 1:
                break
            elif auto_thresh_dict[f'{x_thresh}, {y_thresh}'] == 999:
                all_y_test = all_y_test[y_index:]
            else:
                all_y_test = all_y_test[:-y_index]

        if len(all_x_test) == 1:
                break
        elif auto_thresh_dict[f'{x_thresh}, {y_thresh}'] < 999:
            all_x_test = all_x_test[x_index:]
            # print('first half')
        else:
            all_x_test = all_x_test[:x_index]
            # print('second half')
            
    
    to_plot = np.array(to_plot)

    x_vals = to_plot[:, 0:1]
    y_vals = to_plot[:, 1:2]
    z_vals = to_plot[:, 2:3]
    mask = np.where(z_vals != 999)

    fig, ax = plt.subplots()
    im = ax.scatter(x_vals[mask], y_vals[mask], c = z_vals[mask])
    mask = np.where(z_vals == 999)
    ax.scatter(x_vals[mask], y_vals[mask], c = 'red')

    for i,j,k in zip(x_vals, y_vals, z_vals):
        ax.annotate(str(k),xy=(i,j), xytext=(0,10), textcoords='offset points')

    fig.colorbar(im, orientation='vertical')
    plt.show(block=True)
    # z_vals[mask] = z_vals[mask] - (np.max(z_vals) - 0.4)

    # print(np.unravel_index(z_vals.argmin(), z_vals.shape))

    x_thresh = x_vals[np.argmin(z_vals)]
    y_thresh = y_vals[np.argmin(z_vals)]

    return x_thresh, y_thresh

def allthresh(folder_path, pairs, pair_data, pixel_positions, tiles, unknown_images, camera_to_sample, resize, fractional_overlap, starting_pixel_positions, scan_centre, img_limit, downsample, all_x, all_y):
    auto_thresh_dict = {}

    t1 = time.time()

    print(len(pairs))

    all_x = sorted(list(set(all_x)), reverse = False)
    all_y = sorted(list(set(all_y)), reverse = False)

    to_plot = []
    cached_pairs = {}
    with alive_bar(len(all_x), force_tty=True, dual_line=False) as bar:  # declare your expected total
        for x_thresh in all_x:
            this_col = []
            y_thresh = np.max(all_y)
            accepted_displacements, accepted_pairs = reconstruct_tiled_image.verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh)
            auto_thresh_dict[f'{x_thresh}, {y_thresh}'] = test_thresh(accepted_pairs, pixel_positions, tiles, accepted_displacements)
            to_plot.append([x_thresh, y_thresh, auto_thresh_dict[f'{x_thresh}, {y_thresh}']])
            cached_pairs[str(accepted_pairs)] = to_plot[-1][-1]
            if to_plot[-1][-1] == 999:
                print('max breaks')
                pass
            else:
                for y_thresh in all_y:
                    accepted_displacements, accepted_pairs = reconstruct_tiled_image.verify_overlaps(pairs, pair_data, pixel_positions, x_thresh, y_thresh)
                    pairs_hash = str(accepted_pairs)

                    if pairs_hash in list(cached_pairs.keys()):
                        auto_thresh_dict[f'{x_thresh}, {y_thresh}'] = cached_pairs[pairs_hash]
                        to_plot.append([x_thresh, y_thresh, auto_thresh_dict[f'{x_thresh}, {y_thresh}']])
                        this_col.append(to_plot[-1][-1])

                    else:
                        auto_thresh_dict[f'{x_thresh}, {y_thresh}'] = test_thresh(accepted_pairs, pixel_positions, tiles, accepted_displacements)
                        to_plot.append([x_thresh, y_thresh, auto_thresh_dict[f'{x_thresh}, {y_thresh}']])
                        this_col.append(to_plot[-1][-1])
                        cached_pairs[str(accepted_pairs)] = to_plot[-1][-1]
                        # if auto_thresh_dict[f'{x_thresh}, {y_thresh}'] == 999:
                        #     break
                    try:
                        if this_col[-1] - this_col[-2] > 0:
                            break
                    except:
                        pass
            # print(this_col)
            # if len(this_col)>1:
            #     diffs = np.diff(this_col)
            #     if np.min(this_col) != this_col[np.argmax(diffs>0)]:
            #         print('not the same')
            #         print(np.min(this_col), this_col[np.argmax(diffs>0)])
            #         plt.plot(this_col,'.')
            #         plt.show(block=True)
            # else:
            #     if this_col[0] == 999:
            #         break

            bar()
    del this_col
    del auto_thresh_dict
    print(time.time() - t1)
    
    to_plot = np.array(to_plot)

    x_vals = to_plot[:, 0:1]
    y_vals = to_plot[:, 1:2]
    z_vals = to_plot[:, 2:3]
    # mask = np.where(z_vals != 999)

    # fig, ax = plt.subplots()
    # im = ax.scatter(x_vals[mask], y_vals[mask], c = z_vals[mask])
    # mask = np.where(z_vals == 999)
    # ax.scatter(x_vals[mask], y_vals[mask], c = 'red')

    # for i,j,k in zip(x_vals, y_vals, z_vals):
    #     ax.annotate(str(np.round(k,1)),xy=(i,j), xytext=(0,10), textcoords='offset points')

    # fig.colorbar(im, orientation='vertical')
    # plt.show(block=True)
    # z_vals[mask] = z_vals[mask] - (np.max(z_vals) - 0.4)

    # print(np.unravel_index(z_vals.argmin(), z_vals.shape))

    x_thresh = x_vals[np.argmin(z_vals)]
    y_thresh = y_vals[np.argmin(z_vals)]

    return x_thresh, y_thresh
import tiff_tile
if __name__ in "__main__":
    # for folders in os.listdir(sys.argv[1]):
    #     folder = os.path.join(sys.argv[1], folders)
    #     find_overlaps(folder)
        
    #     tile_compare.compare(folder)
    start = time.time()
    mem_usage = memory_usage((find_overlaps, (sys.argv[1], ),  ))
    tiff_tile.produce_tiff(os.path.join(sys.argv[1]))#, 'split'))
    print('Maximum memory usage: %s' % max(mem_usage))
    print(f'Total time taken is {datetime.timedelta(seconds = round(time.time()-start))} minutes:seconds')
    plt.plot(mem_usage)
    plt.show(block=True)
